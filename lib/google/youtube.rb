# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

# token='ya29.a0AcM612zwgxUO6hokvN5QePOVUGib-i0KPpNInBEACalb3dhJ7iJBVGpAWLxMzCie9k01brt2RNcR8FsKkrrRhmurg43xVnm1HPP-Djq8PxO7-TbrIGcDvFHzjAWWEZQilkFQstP5UFzYf48dqDyrcOPNdmu_iJjiikOjmgaCgYKAdoSARASFQHGX2MiLpRypbv26P-JRhSmRhjLhQ0173'
# api_key='AIzaSyCROae3vyGo07j6rEQXpOt35aGwxe577d0'
# playlist_item_id='UExCQUU5MzcyODg4RjVBOUI0LjI1RDI2QkJGNzFBM0RBMTY'
# url="https://www.googleapis.com/youtube/v3/playlistItems?id=${playlist_item_id}&key=${api_key}"
# curl --verbose "${url}" --header "Authorization: Bearer ${token}"
# curl --verbose --request DELETE "${url}" --header "Authorization: Bearer ${token}"


require 'google/apis/youtube_v3'

require_relative 'authorization'

module YouTubeAPI
  PERMISSIONS = {
    default: Google::Apis::YoutubeV3::AUTH_YOUTUBE_READONLY,
    rw: [
      Google::Apis::YoutubeV3::AUTH_YOUTUBE,
      Google::Apis::YoutubeV3::AUTH_YOUTUBE_READONLY
    ]
  }

  def authorize_for(permissions)
    @credentials ||= begin
      credentials = Google::Authorization.new.authorize(permissions)
      @refresh_token = credentials.refresh_token
      credentials
    end
  end
  module_function :authorize_for

  def client(permission = :default)
    # Set up the YouTube Data API client
    @service ||= begin
      service = Google::Apis::YoutubeV3::YouTubeService.new
      service.client_options.application_name = APPLICATION_NAME
      service.authorization = authorize_for(PERMISSIONS[permission])

      service.list_playlists('id', mine: true, max_results: 1)

      service
    rescue Signet::AuthorizationError => e
      message = JSON.parse(e.response&.body || '{}')['error_description']
      log.error "Authorization error: #{message}"
      exit
    end
  end
  module_function :client
end
