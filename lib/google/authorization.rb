# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'fileutils'

require 'googleauth'
require 'googleauth/stores/file_token_store'

require_relative 'secret_store'

module Google
  class Authorization
    attr_reader :client_id, :credentials_path, :secret_store, :token_store

    MODULE_DIR_PATH = File.expand_path(__dir__)
    LIB_DIR_PATH = File.expand_path(File.dirname(MODULE_DIR_PATH))
    PROJECT_DIR_PATH = File.expand_path(File.dirname(LIB_DIR_PATH))
    AUTH_REQUIRES_STDIN = "Please don't pipe things to #{$PROGRAM_NAME} " \
      "until authorization is complete".freeze

    def initialize
      credentials_dir_path = File.join(ENV['HOME'], '.credentials', 'youtube')
      FileUtils.mkdir_p(credentials_dir_path)
      @secret_path = File.join(credentials_dir_path, 'secret.yaml')
      @credentials_path = File.join(credentials_dir_path, 'token.yaml')
      @secret_store = SecretStore.new(file: @secret_path)
      @client_id = Google::Auth::ClientId.new(@secret_store.client_id, @secret_store.client_secret)
      @token_store = Google::Auth::Stores::FileTokenStore.new(file: @credentials_path)
    end

    def delete_token
      if File.exist?(@credentials_path)
        File.delete(@credentials_path)
        puts "Deleted token file: #{@credentials_path}"
      else
        puts "Deletion failed -- file not found: #{@credentials_path}"
      end
    end

    # Set up OAuth 2.0 credentials
    # See: https://console.cloud.google.com/apis/credentials?project=api-project-772291154606
    def authorize(permission)
      authorizer = Google::Auth::UserAuthorizer.new(
        client_id,
        permission,
        token_store
      )

      credentials = authorizer.get_credentials(@secret_store.user_id)
      loop do
        if credentials.nil?
          url = authorizer.get_authorization_url(base_url: 'urn:ietf:wg:oauth:2.0:oob')

          puts 'Open the following URL in your browser to authorize:'
          puts url
          puts 'Enter the authorization code:'
          code = $stdin.gets&.chomp
          raise StandardError, AUTH_REQUIRES_STDIN if code.nil?

          credentials = authorizer.get_and_store_credentials_from_code(
            user_id: @secret_store.user_id, code: code, base_url: 'urn:ietf:wg:oauth:2.0:oob'
          )
        end

        if !credentials.nil? && credentials.expired? && credentials.refresh_token
          begin
            credentials.refresh!
          rescue Signet::AuthorizationError => e
            error = JSON.parse(e.response&.body || '{}')['error_description']
            if error == 'Token has been expired or revoked.'
              credentials = nil
              next
            end
          end
        end
        break
      end

      credentials
    end
  end
end
