# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

require_relative '../google/youtube'
require_relative '../database'

PlaylistItems = Module.new unless defined?(PlaylistItems)

module PlaylistItems::InstanceMethods
  def initialize(options = {})
    @options = options
    DB.loggers << logger if options[:sql_logging]
    @playlist_items = options[:playlist_items]
    YouTubeAPI.client # initialize and authorize the client
  end

  def logger
    require 'logger'
    Logger.new($stdout, level: Logger::DEBUG)
  end

  def update_playlist_items
    @known_playlist_items = PlaylistItem.all
    @known_playlist_item_identifiers = known_playlist_items.map{ |x| x[:source_id] }

    log.info "Known playlist items: #{known_playlist_items.length}"
    # known_playlist_items.each do |playlist_item|
    #   log.debug JSON.pretty_generate(playlist_item.values)
    #   log.debug "------------------------"
    # end

    Playlist.each do |playlist|
      log.info "Reading playlist items for playlist: #{playlist.title}"
      read_playlist_items_by_playlist(playlist.source_id)
    end
  end

  def read_playlist_items(items = @playlist_items, parts = 'id,snippet,contentDetails,status')
    playlist_items = []

    # Fetch the playlist items
    begin
      items.each do |item|
        log.info "Fetching playlist item: #{item}"
        response = YouTubeAPI.client.list_playlist_items(parts, id: item)

        if response.items.any?
          playlist_items.concat(response.items)
        else
          log.warn "No playlist item found for id: #{item}"
        end
      end
    rescue Google::Apis::ClientError => e
      error_body = JSON.parse(e.body)
      if e.status_code == 404 && error_body['error']['errors'].any? { |err| err['reason'] == 'playlistNotFound' }
        log.error "Error: Playlist not found: #{playlist_id}"
      else
        log.error "Error reading playlist items: #{e.message}"
      end
    end

    unless playlist_items.empty?
      log.debug "Example playlist item:"
      log.debug playlist_items.first.content_details.video_id + '  ' + playlist_items.first.snippet.title
    end

    playlist_items
  end

  def fetch_playlist_items(playlist_id, parts)
    playlist_items = []

    log.info "Fetching playlist items for playlist: #{playlist_id}"

    # Fetch the playlist items
    begin
      response = YouTubeAPI.client.list_playlist_items(parts, playlist_id: playlist_id)

      return playlist_items if response.items.empty?

      first_playlist_item = response.items.first

      loop do
        playlist_items.concat(response.items)
        break if response.next_page_token.nil?
        response = YouTubeAPI.client.list_playlist_items(
          parts, playlist_id: playlist_id, page_token: response.next_page_token)
      end
    rescue Google::Apis::ClientError => e
      error_body = JSON.parse(e.body)
      if e.status_code == 404 && error_body['error']['errors'].any? { |err| err['reason'] == 'playlistNotFound' }
        log.error "Error: Playlist not found: #{playlist_id}"
      else
        log.error "Error reading playlist items: #{e.message}"
      end
    end

    log.debug "Example playlist item:"
    log.debug first_playlist_item.content_details.video_id + '  ' + first_playlist_item.snippet.title

    playlist_items
  end

  def read_playlist_items_by_playlist(playlist_id, parts = 'id,snippet,contentDetails,status')
    playlist_items = fetch_playlist_items(playlist_id, parts)
    novel_playlist_items = []

    playlist_items.each do |playlist_item|
      next if known_playlist_item_identifiers.include?(playlist_item.id)
      novel_playlist_items << PlaylistItem.create(
        source_id: playlist_item.id,
        playlist_id: playlist_item.snippet.playlist_id,
        video_id: playlist_item.content_details.video_id,
        title: playlist_item.snippet.title,
        published_at: playlist_item.snippet.published_at
      )
    end

    if novel_playlist_items.empty?
      log.info "No new playlist items were detected"
    else
      log.info "New playlist items:"
      novel_playlist_items.each do |item|
        log.info item.video_id + '  ' + item.title
      end
    end

    return playlist_items
  rescue Google::Apis::ClientError => e
    log.error "Error reading playlist items: #{e.message}"
  end
end

class PlaylistItems::Reader
  attr_reader :known_playlist_items, :known_playlist_item_identifiers
  include PlaylistItems::InstanceMethods
end
