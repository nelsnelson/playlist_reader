# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'logger'
require 'optparse'

require_relative '../version'

# The Options module
module Options
  DEFAULTS = {
    limit: 1,
    log_level: Logger::INFO,
    sql_logging: false
  }.freeze

  # The ArgumentsParser
  class ArgumentsParser
    attr_reader :parser, :options

    def initialize(args, defaults = Options::DEFAULTS, option_parser = OptionParser.new, **params)
      @args = args
      @params = params
      @parser = option_parser
      @options = defaults.dup
      flags.each { |method_name| self.method(method_name).call }
    end

    def parse
      @parser.parse!(@args, **@params)
      @options[:playlist_items] = $stdin.tty? ? @args : $stdin.read.split("\n")
      @options[:playlist_items].delete_if { |s| s.strip.empty? }
    end

    def flags
      @flags ||= %i[banner live_run limit verbose quiet sql_logging help version]
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [playlist_item_ids, ...] [options]"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def live_run
      @parser.on_tail('--live-run', 'Enable a live-run; default: simulation only') do
        @options[:live_run] = true
      end
    end

    def handle_no_limit(limit)
      limit < 1 ? Float::INFINITY : limit
    end

    def limit
      @parser.on_tail('--limit=<N>', "Only download metadata for N playlist media items; default: #{@options[:limit]}") do |v|
        @options[:limit] = handle_no_limit(v.to_i)
      end
    end

    def verbose
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] -= 1
      end
    end

    def quiet
      @parser.on_tail('-q', '--quiet', 'Decrease verbosity') do
        @options[:log_level] += 1
      end
    end

    def sql_logging
      @parser.on_tail('--log-sql', 'Display SQL statements logging; default: false') do
        @options[:sql_logging] = true
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{$PROGRAM_NAME} version #{VERSION}"
        exit
      end
    end
  end
  # class ArgumentsParser

  def parse_arguments(defaults = Options::DEFAULTS, args = ARGV, _file_path = ARGF)
    arguments_parser = ArgumentsParser.new(args, defaults)
    arguments_parser.parse
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
    puts e.message
    puts arguments_parser.parser
    exit
  rescue OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module Options
