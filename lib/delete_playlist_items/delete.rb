# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

require_relative '../google/youtube'
require_relative '../database'

PlaylistItems = Module.new unless defined?(PlaylistItems)

module PlaylistItems::InstanceMethods
  PERMISSION_DENIED = %r{PERMISSION_DENIED}.freeze
  NOT_FOUND = %r{Not Found}.freeze

  def initialize(options = {})
    @options = options
    DB.loggers << logger if options[:sql_logging]
    @limit = options[:limit]
    @playlist_items = options[:playlist_items]
    @local_always = options[:local_always]
    @live_run = options[:live_run]
    @batch_size = options[:batch_size]
    @max_retries = options[:max_retries]
    @retry_delay_seconds = options[:retry_delay_seconds]
    YouTubeAPI.client(:rw) # initialize and authorize the client
  end

  def logger
    require 'logger'
    Logger.new($stdout, level: Logger::DEBUG)
  end

  def delete_playlist_items(
      playlist_items = @playlist_items, batch_size = @batch_size,
      max_retries = @max_retries, retry_delay_seconds = @retry_delay_seconds,
      local_always = @local_always, live_run = @live_run)

    deleted_items = []

    if live_run

    playlist_items.each_slice(batch_size) do |batch|
      batch.each do |item|
        retries = 0
        begin
          log.debug "Deleting playlist item: #{item}"
          response = YouTubeAPI.client.delete_playlist_item(item)
          if response == '' || (response.respond_to?(:success?) && response.success?)
            deleted_items << item
            log.info "Deleted playlist item: #{item}"
          else
            deleted_items << item if local_always
            log.info "Failed playlist item deletion: #{item}"
          end
        rescue => e
          if e.respond_to?(:header) && NOT_FOUND.match?(e.header.reason_phrase)
            log.warn "Playlist item not found: #{item} (#{e.message})"
            next
          end
          raise e if PERMISSION_DENIED.match?(e.message)
          if retries < max_retries
            delay = retry_delay_seconds ** retries
            log.warn "Retrying failed playlist item deletion: #{e.message}"
            sleep(delay)
            retries += 1
            retry
          else
            log.error "Failed playlist item deletion after #{retries} " \
              "attempts: #{e.message}"
          end
        end
      end
    end

    else
      log.info "[Dry-run] Would have attempted to delete " \
        "#{playlist_items.length} remote playlist items"
      deleted_items.concat(playlist_items) if local_always
    end

    # Bulk deletion from the database after all API deletions are complete
    query = PlaylistItem.where(source_id: deleted_items)
    hits = query.count
    if live_run
      query.delete
      log.info "Deleted #{hits} local playlist items"
    else
      log.info "[Dry-run] Would have deleted #{hits} local playlist items"
    end
  end
end
# module PlaylistItems::InstanceMethods

class PlaylistItems::Deleter
  include PlaylistItems::InstanceMethods
end
