# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'logger'
require 'optparse'

require_relative '../version'

# The Options module
module Options
  DEFAULTS = {
    limit: -1,
    log_level: Logger::INFO,
    playlist_items: [],
    batch_size: 50,
    max_retries: 3,
    retry_delay_seconds: 2,
    local_always: false,
    live_run: false,
    sql_logging: false
  }.freeze

  # The ArgumentsParser
  class ArgumentsParser
    attr_reader :parser, :options
    FLAGS = %i[
      banner batch_size max_retries retry_delay_seconds local live_run
      verbose quiet sql_logging help version
    ]

    def initialize(
        args, file_path = nil, defaults = Options::DEFAULTS,
        option_parser = OptionParser.new, **params)
      @args = args
      @file_path = file_path
      @params = params
      @parser = option_parser
      @options = defaults.dup
      FLAGS.each { |method_name| self.method(method_name).call }
    end

    def parse
      @parser.parse!(@args, **@params)
      @options[:playlist_items] = $stdin.tty? ? @args : $stdin.read.split("\n")
      @options[:playlist_items].delete_if { |s| s.strip.empty? }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options] [playlist_items...]\n" \
        "\nRemoves items deleted by original uploader from your playlists."
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def batch_size
      description = "Batch size; default: #{@options[:batch_size]}"
      @parser.on_tail('-s', '--batch-size', description) do |v|
        @options[:batch_size] = v.to_i
      end
    end

    def max_retries
      description = "Maximum retries; default: #{@options[:max_retries]}"
      @parser.on_tail('-r', '--max-retries', description) do |v|
        @options[:max_retries] = v.to_i
      end
    end

    def retry_delay_seconds
      description = "Retry delay in seconds; default: #{@options[:retry_delay_seconds]}"
      @parser.on_tail('-d', '--retry-delay', description) do |v|
        @options[:retry_delay_seconds] = v.to_i
      end
    end

    def local
      description = "Always delete local persisted record; default: #{@options[:local_always]}"
      @parser.on_tail('--local', description) do
        @options[:local_always] = true
      end
    end

    def live_run
      @parser.on_tail('--live-run', 'Enable a live-run; default: simulation only') do
        @options[:live_run] = true
      end
    end

    def verbose
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] -= 1
      end
    end

    def quiet
      @parser.on_tail('-q', '--quiet', 'Decrease verbosity') do
        @options[:log_level] += 1
      end
    end

    def sql_logging
      @parser.on_tail('--log-sql', 'Display SQL statements logging; default: false') do
        @options[:sql_logging] = true
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{$PROGRAM_NAME} version #{VERSION}"
        exit
      end
    end
  end
  # class ArgumentsParser

  def parse_arguments(defaults = Options::DEFAULTS, args = ARGV, file_path = ARGF)
    arguments_parser = ArgumentsParser.new(args, file_path, defaults)
    arguments_parser.parse
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
    puts e.message
    puts arguments_parser.parser
    exit
  rescue OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module Options
