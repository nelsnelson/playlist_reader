# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

require_relative '../database'

Playlists = Module.new

module Playlists::InstanceMethods
  COLUMNS = {
    id: 'ID',
    title: 'Title',
    published_at: 'Published At'
  }.freeze

  def initialize(options = {})
    @options = options
    if options[:sql_logging]
      require 'logger'
      DB.loggers << Logger.new($stdout, level: Logger::DEBUG)
    end
    @quiet = options[:quiet]
  end

  def including_column_names(items)
    return items if items.empty?
    [COLUMNS.select { |key, _| items.first.keys.include?(key) }] + items
  end

  def list_playlists
    query = Playlist.order(:title).select(:source_id, :title, :published_at)
    playlists = query.all
    max_source_id_length = playlists.map { |playlist| playlist[:source_id].length }.max
    max_title_length = playlists.map { |playlist| playlist[:title].length }.max
    ([{source_id: 'ID', title: 'Title', published_at: 'Published'}] + playlists)
    playlists = including_column_names(playlists) unless @quiet
    playlists.each do |playlist|
      puts format(
        "%-#{max_source_id_length}s %-#{max_title_length}s %-s",
        playlist[:source_id],
        playlist[:title],
        playlist[:published_at])
    end
  end
end

class Playlists::Lister
  include Playlists::InstanceMethods
end
