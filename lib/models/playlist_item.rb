# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

DB.create_table?(:playlist_items) do
  primary_key :id
  String :source_id, unique: true, null: false
  String :video_id, null: false
  String :playlist_id
  String :title
  TrueClass :downloaded, default: false
  DateTime :created_at
  DateTime :published_at
  index :source_id
  index :video_id
  index :playlist_id
  index :title
  index :created_at
  index :published_at
end

class PlaylistItem < Sequel::Model
  plugin :timestamps, create: :created_at, update: false

  # This hook will automatically set the created_at timestamp
  def before_create
    self.created_at = Time.now
    super
  end
end
