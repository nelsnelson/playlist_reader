# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

require_relative '../google/youtube'
require_relative '../database'

Playlists = Module.new unless defined?(Playlists)

module Playlists::InstanceMethods
  def initialize(options = {})
    @options = options
    DB.loggers << logger if options[:sql_logging]
    YouTubeAPI.client # initialize and authorize the client
  end

  def logger
    require 'logger'
    Logger.new($stdout, level: Logger::DEBUG)
  end

  def create_playlists(playlists)
    novel_playlists = []
    playlists.each do |playlist|
      novel_playlists << Playlist.create(
        source_id: playlist.id,
        title: playlist.snippet.title,
        published_at: playlist.snippet.published_at
      )
    end
    novel_playlists
  end

  def update_playlists(playlists)
    existing_playlists = []
    playlists.each do |playlist|
      existing_playlist = Playlist.where(source_id: playlist.id).first
      next unless existing_playlist
      existing_playlist.update(
        title: playlist.snippet.title,
        published_at: playlist.snippet.published_at
      )
      existing_playlists << existing_playlist
    end
    existing_playlists
  end

  def read_playlists
    known_playlists = Playlist.all
    known_playlist_identifiers = known_playlists.map{ |x| x[:source_id] }

    log.debug "Known playlists:"
    log.debug known_playlists.map(&:title).sort.join(', ')
    # known_playlists.each do |playlist|
    #   log.debug JSON.pretty_generate(playlist.values)
    #   log.debug "------------------------"
    # end

    playlists = []
    novel_playlists = []

    log.info "Fetching playlists..."

    # Fetch playlists for your user
    response = YouTubeAPI.client.list_playlists(
      'snippet,contentDetails', mine: true)

    first_playlist = response.items.first

    loop do
      playlists.concat(response.items)
      break if response.next_page_token.nil?
      response = YouTubeAPI.client.list_playlists(
        'snippet,contentDetails', mine: true,
        page_token: response.next_page_token)
    end

    log.debug "Example playlist:"
    log.debug first_playlist.id + '  ' + first_playlist.snippet.title + '  ' + \
      first_playlist.snippet.published_at.to_s
    # log.debug JSON.pretty_generate(first_playlist)

    # Fetch Watch Later and Liked Videos playlists
    # ['WL', 'LL'].each do |playlist_id|
    #   response = YouTubeAPI.client.list_playlist_items(
    #     'snippet,contentDetails', playlist_id: playlist_id)
    #   log.debug JSON.pretty_generate(response.items.first)
    #   loop do
    #     if known_playlist_identifiers.empty?
    #       playlists.concat(response.items)
    #     else
    #       playlists.concat(
    #         response.items.reject { |playlist|
    #           known_playlist_identifiers.include?(playlist.id)
    #         }
    #       )
    #     end
    #     break if response.next_page_token.nil?
    #     response = YouTubeAPI.client.list_playlist_items(
    #       'snippet,contentDetails', playlist_id: playlist_id,
    #       page_token: response.next_page_token)
    #   end
    # end


    if known_playlist_identifiers.empty?
      novel_playlists.concat(playlists)
    else
      novel_playlists.concat(
        response.items.select do |playlist|
          !known_playlist_identifiers.include?(playlist.id)
        end
      )
    end

    if novel_playlists.empty?
      log.info "No new playlists detected"
    else
      novel_playlists = create_playlists(novel_playlists)
      log.info "New playlists:"
      log.info novel_playlists.map(&:title).sort.join(', ')
    end

    known_playlists = update_playlists(playlists)

    return novel_playlists + known_playlists
  rescue Google::Apis::ClientError => e
    log.error "Error reading playlists: #{e.message}"
  end
end

class Playlists::Reader
  include Playlists::InstanceMethods
end
