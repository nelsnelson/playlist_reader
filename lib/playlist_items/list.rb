# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'json'

require_relative '../database'

module PlaylistItems
  COLUMNS = {
    id: 'ID',
    video_id: 'Video ID',
    playlist_title: 'Playlist',
    playlist_item_title: 'Title'
  }.freeze
  FIELD_LENGTHS = {
    id: 0, video_id: 0, playlist_id: 0, playlist_title: 0
  }.freeze

  module InstanceMethods
    def initialize(options = {})
      @options = options
      DB.loggers << logger if options[:sql_logging]
      @limit = options[:limit]
      @deleted_only = options[:deleted_only]
      @playlist = options[:playlist]
      @log_level = options[:log_level]
      @playlist_items = playlist_items
    end

    def logger
      require 'logger'
      Logger.new($stdout, level: Logger::DEBUG)
    end

    def playlist_items(playlist = @playlist, deleted_only = @deleted_only, limit = @limit)
      selectors = [
        Sequel[:playlist_items][:source_id].as(:id),
        Sequel[:playlist_items][:video_id],
        Sequel[:playlist_items][:title].as(:playlist_item_title)
      ]
      selectors << Sequel[:playlists][:title].as(:playlist_title) if playlist.nil?
      query = PlaylistItem.select(*selectors)
      query = query.join(:playlists, source_id: :playlist_id)
      query = query.order(
        Sequel[:playlists][:title],
        Sequel[:playlist_items][:title]
      )

      query = query.where(Sequel[:playlist_items][:title] => "Deleted video") if deleted_only
      unless playlist.nil?
        query = query.where(
          Sequel.|(
            { Sequel[:playlist_items][:playlist_id] => playlist },
            { Sequel[:playlists][:title] => playlist }
          )
        )
      end
      query = query.limit(limit) if limit.positive?
      query.all
    end

    def including_column_names(items)
      return items if items.empty?
      [COLUMNS.select { |key, _| items.first.keys.include?(key) }] + items
    end

    def list_playlist_items(playlist_items = @playlist_items, quiet = @log_level > 1)
      playlist_items.compact!
      playlist_items = including_column_names(playlist_items) unless quiet
      max_lengths = playlist_items.each_with_object(FIELD_LENGTHS.dup) do |item, lengths|
        lengths[:id] = [lengths[:id], item[:id].to_s.length].max
        lengths[:video_id] = [lengths[:video_id], item[:video_id].length].max
        lengths[:playlist_title] = [lengths[:playlist_title], item[:playlist_title]&.length.to_i].max
      end

      template = ["%-#{max_lengths[:id]}s"]
      unless quiet
        sample = playlist_items.first
        template.insert(1, *[
          "%-#{max_lengths[:video_id]}s",
          "%-#{max_lengths[:playlist_title]}s"
        ]) if !sample.nil? && sample.keys.length > 2
        template << '%-s'
      end

      playlist_items.each do |item|
        values = [item[:id]]
        unless quiet
          if !item.nil? && item.keys.length > 2
            values << item[:video_id]
            values << item[:playlist_title]
          end
          values << item[:playlist_item_title]
        end
        puts format(template.join('  '), *values).strip
      end
    end

  end
  # module InstanceMethods
end
# module PlaylistItems

class PlaylistItems::Lister
  include PlaylistItems::InstanceMethods
end
