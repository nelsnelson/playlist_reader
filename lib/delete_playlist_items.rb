#! /usr/bin/env ruby
# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'logging'
require_relative 'delete_playlist_items/delete'
require_relative 'delete_playlist_items/options'

APPLICATION_NAME = 'playlist_reader_client'

class App
  include Options

  def initialize(args = parse_arguments)
    Logging.level = args[:log_level]
    PlaylistItems::Deleter.new(args).delete_playlist_items
  rescue Interrupt => _e
    abort "\rInterrupted"
  rescue StandardError => e
    abort e.message
  end
end

App.new if $PROGRAM_NAME == __FILE__
