#! /usr/bin/env ruby
# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'logging'
require_relative 'google/authorization'
require_relative 'google/options'

APPLICATION_NAME = 'playlist_reader_client'

class App
  include Options

  def initialize(args = parse_arguments(DEFAULTS))
    Logging.level = args[:log_level]
    auth = Google::Authorization.new
    unless args[:live_run]
      puts "[Dry-run] Would have deleted token file: #{auth.credentials_path}"
      exit
    end

    auth.delete_token
  rescue Interrupt => _e
    abort "\rInterrupted"
  end
end

App.new if $PROGRAM_NAME == __FILE__
