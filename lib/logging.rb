# encoding: utf-8
# frozen_string_literal: false

# Copyright Nels Nelson 2023 but freely usable (see license)
#
# This application is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this application.  If not, see <http://www.gnu.org/licenses/>.

require 'logger'
require 'syslog/logger'

PROJECT = File.basename(__dir__) unless defined?(PROJECT)

# Define class MultiLogger
class MultiLogger
  TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S.%L'.freeze
  FORMAT = "%<timestamp>s %-5<severity>s [%<progname>s] %<msg>s\n".freeze

  def initialize(invoking_class)
    @invoking_class = invoking_class
  end

  %i[info warn debug error fatal].each do |method|
    define_method(method) do |message|
      MultiLogger.loggers.each do |logger|
        logger.add(Logger.const_get(method.upcase), message, @invoking_class || 'Unknown')
      end
    end
  end

  def log_level=(level)
    MultiLogger.loggers.each { |logger| logger.level = level }
  end

  def self.formatter(severity, timestamp, progname, msg)
    format(
      MultiLogger::FORMAT,
      timestamp: timestamp.strftime(MultiLogger::TIMESTAMP_FORMAT),
      severity: severity, progname: progname, msg: msg)
  end

  def self.loggers
    @loggers ||= begin
      stdout_logger = Logger.new($stdout)
      stdout_logger.level = Logger::INFO
      stdout_logger.formatter = self.method(:formatter)
      syslog_logger = Syslog::Logger.new(PROJECT)
      syslog_logger.level = Logger::INFO
      [stdout_logger, syslog_logger]
    end
  end
end

module Logging
  def level=(level)
    MultiLogger.loggers.each { |logger| logger.level = level }
  end
  module_function :level=
end

module LogExt
  def log
    @log ||= MultiLogger.new(self.class.name)
  end
end

Object.include(LogExt)
Module.include(LogExt)
Class.include(LogExt)
