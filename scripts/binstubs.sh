#! /usr/bin/env bash

# Exit if any command returns a non-zero exit status
set -e

# Exit if an unset variable is used
set -u

# Exit if any command in a series of piped commands fails
set -o pipefail

# Ensure the target directory exists
mkdir -p ./bin

# Define the source directory (passed as an argument or default to ./lib)
SOURCE_DIR=${1:-./lib}

# Find all executable Ruby files in the source directory (non-recursive)
find "$SOURCE_DIR" -maxdepth 1 -type f -name "*.rb" -perm -u=x | while read -r ruby_file; do
  # Extract the base name of the Ruby file (without extension)
  filename=$(basename "$ruby_file" .rb)

  # Create a binstub in the ./bin directory
  binstub_path="./bin/$filename"

  cat <<EOF > "$binstub_path"
#! /usr/bin/env ruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

require_relative '.${SOURCE_DIR}/$filename'

App.new if \$PROGRAM_NAME == __FILE__
EOF

  # Make the binstub executable
  chmod +x "$binstub_path"

  echo "Generated binstub for $ruby_file at $binstub_path"
done
