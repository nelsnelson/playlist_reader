# playlist_reader

Reads playlists.

Currently only supports Google (YouTube) playlist data.


## Install

Installation instructions follow.


### Install mise-en-place

The [mise] CLI tool is used to manage multiple runtime versions.

```sh
curl https://mise.jdx.dev/install.sh | sh
~/.local/bin/mise --version
mise 2024.x.x
```

Enable mise activation in future zsh sessions.

```sh
echo 'eval "$(~/.local/bin/mise activate zsh)"' >> ~/.zshrc
```


### Install required runtime software

Use mise to install the runtime software defined as requirements
in the .tool-versions file.

```sh
mise install
```

Install [bundler].

```sh
gem install bundler
```


## Usage

Usage instructions follow.


### Fetch playlists

Pull down your playlists.

```sh
bundle exec bin/playlists_reader
```

Now that you have persisted your playlists to the local datastore, pull
down the items in those playlists.

```sh
bundle exec bin/playlist_items_reader
```

You may also list the known playlists.

```sh
bundle exec bin/playlists
```

Or list the known playlist items for a given playlist identifier.

```sh
bundle exec bin/playlist_items --playlist="Music 2024-03-23"
```

You may also delete playlist items which have been removed by the original
uploading user.

```sh
bundle exec bin/delete_playlist_items --help
bundle exec bin/delete_playlist_items --live-run
```


[mise]: https://mise.jdx.dev/getting-started.html
[ruby]: https://www.ruby-lang.org/en/
[bundler]: https://bundler.io
