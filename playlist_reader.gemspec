# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require_relative 'lib/version'

PROJECT = File.basename(__dir__) unless defined?(PROJECT)
UNDERSCORE_PATTERN = '_'.freeze unless defined?(UNDERSCORE_PATTERN)
HYPHEN_STRING = '-'.freeze unless defined?(HYPHEN_STRING)

# rubocop: disable Gemspec/RequiredRubyVersion
# rubocop: disable Metrics/AbcSize
# rubocop: disable Metrics/MethodLength
def gem_spec
  Gem::Specification.new do |spec|
    spec.name = PROJECT.gsub(UNDERSCORE_PATTERN, HYPHEN_STRING)
    spec.version = ::VERSION
    spec.summary = 'Download YouTube Playlists.'
    spec.description = 'YouTube Playlists downloader.'
    spec.authors = ['Nels Nelson']
    spec.email = 'nels@nelsnelson.org'
    spec.files = `git ls-files -z`.split("\x0").select do |f|
      f.match(%r{^(bin/|lib/|man/|README.md|LICENSE)})
    end
    spec.bindir = 'bin'
    spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
    spec.homepage = "https://rubygems.org/gems/#{PROJECT}"
    spec.metadata = {
      'source_code_uri' => "https://gitlab.com/nelsnelson/#{PROJECT}",
      'bug_tracker_uri' => "https://gitlab.com/nelsnelson/#{PROJECT}/issues",
      'rubygems_mfa_required' => 'true'
    }
    spec.license = 'MIT'

    spec.require_paths = ['lib']
    spec.required_ruby_version = '>= 2.6.8'
    spec.add_dependency 'google-apis-youtube_v3'
    spec.add_dependency 'googleauth'
    spec.add_dependency 'mutex_m'
    spec.add_dependency 'pstore'
    spec.add_dependency 'sequel'
    spec.add_dependency 'sqlite3'
    spec.add_dependency 'syslog', '~> 0.1.2'
  end
end
# rubocop: enable Gemspec/RequiredRubyVersion
# rubocop: enable Metrics/AbcSize
# rubocop: enable Metrics/MethodLength

GEM_SPEC = gem_spec unless defined?(GEM_SPEC)

GEM_SPEC
